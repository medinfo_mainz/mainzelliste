# Mainzelliste

Mainzelliste is a web-based first-level pseudonymization service. It allows for the creation of personal identifiers (PID) from identifying attributes (IDAT), and thanks to the record linkage functionality, this is even possible with poor quality identifying data. The functions are available through a RESTful web interface.

As of November 2nd, 2016, the Mainzelliste repository has moved to https://bitbucket.org/medicalinformatics/mainzelliste. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Mainzelliste in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).
  